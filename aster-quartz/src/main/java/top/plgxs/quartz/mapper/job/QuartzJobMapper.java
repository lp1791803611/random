package top.plgxs.quartz.mapper.job;

import top.plgxs.quartz.entity.job.QuartzJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 定时任务 Mapper 接口
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-10
 * @version 1.0
 */
public interface QuartzJobMapper extends BaseMapper<QuartzJob> {

    /**
     * 分页数据查询
     * @param quartzJob
     * @return
     * @author Stranger.
     * @date 2021-08-10
     */
    List<QuartzJob> selectQuartzJobList(QuartzJob quartzJob);
}
