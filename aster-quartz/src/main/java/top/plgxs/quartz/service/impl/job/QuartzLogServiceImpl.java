package top.plgxs.quartz.service.impl.job;

import top.plgxs.quartz.entity.job.QuartzLog;
import top.plgxs.quartz.mapper.job.QuartzLogMapper;
import top.plgxs.quartz.service.job.QuartzLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 定时任务日志 服务实现类
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-10
 * @version 1.0
 */
@Service
public class QuartzLogServiceImpl extends ServiceImpl<QuartzLogMapper, QuartzLog> implements QuartzLogService {
    @Resource
    private QuartzLogMapper quartzLogMapper;

    @Override
    public List<QuartzLog> getQuartzLogList() {
        return quartzLogMapper.selectQuartzLogList(null);
    }
}
