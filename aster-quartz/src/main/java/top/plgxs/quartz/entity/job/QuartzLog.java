package top.plgxs.quartz.entity.job;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 定时任务日志
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-10
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("t_quartz_log")
@ApiModel(value="QuartzLog对象", description="定时任务日志")
public class QuartzLog extends Model<QuartzLog> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId("id")
    private String id;

    @ApiModelProperty(value = "任务名称")
    @TableField("job_name")
    private String jobName;

    @ApiModelProperty(value = "调用目标字符串")
    @TableField("invoke_target")
    private String invokeTarget;

    @ApiModelProperty(value = "cron 表达式")
    @TableField("cron_expression")
    private String cronExpression;

    @ApiModelProperty(value = "日志信息")
    @TableField("job_message")
    private String jobMessage;

    @ApiModelProperty(value = "启用状态")
    @TableField("status")
    private String status;

    @ApiModelProperty(value = "异常信息")
    @TableField("exception_message")
    private String exceptionMessage;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
