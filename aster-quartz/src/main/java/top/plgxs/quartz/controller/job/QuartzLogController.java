package top.plgxs.quartz.controller.job;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import top.plgxs.common.core.annotation.Log;
import top.plgxs.common.core.api.ResultInfo;
import top.plgxs.common.core.api.page.PageDataInfo;
import top.plgxs.common.core.constants.enums.BusinessType;
import top.plgxs.quartz.entity.job.QuartzLog;
import top.plgxs.quartz.service.job.QuartzLogService;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 定时任务日志 前端控制器
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-10
 * @version 1.0
 */
@Api(tags = "定时任务日志管理")
@Controller
@RequestMapping("/quartzLog")
public class QuartzLogController {
    @Resource
    private QuartzLogService quartzLogService;

    /**
     * 定时任务日志页面
     * @author Stranger.
     * @since 2021-08-10
     */
    @GetMapping("/list")
    public String list(){
        return "job/quartzLog/list";
    }

    /**
     * 分页查询列表
     * @param searchParams 查询条件
     * @param pageNo 第几页
     * @param pageSize 每页几条
     * @return
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "分页查询定时任务日志列表", notes = "条件查询")
    @GetMapping("/pageList")
    @ResponseBody
    public ResultInfo<PageDataInfo> queryPageList(@RequestParam(name = "searchParams", required = false) String searchParams,
                                                    @RequestParam(name = "page", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "limit", defaultValue = "10") Integer pageSize){
        QueryWrapper<QuartzLog> queryWrapper = new QueryWrapper<>();
        if (StrUtil.isNotBlank(searchParams)) {
            JSONObject jsonObject = JSONObject.parseObject(searchParams);
        }
        queryWrapper.orderByDesc("create_time");
        Page<QuartzLog> page = new Page<>(pageNo, pageSize);
        IPage<QuartzLog> pageList = quartzLogService.page(page, queryWrapper);
        return ResultInfo.success(new PageDataInfo<QuartzLog>(pageList.getRecords(),pageList.getTotal()));
    }

    /**
     * 添加页面
     * @author Stranger.
     * @since 2021-08-10
     */
    @GetMapping("/add")
    public String add(){
        return "job/quartzLog/add";
    }

    /**
     * 插入一条数据
     * @param quartzLog
     * @return top.plgxs.common.api.ResultInfo<java.lang.Object>
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "插入定时任务日志", notes = "插入单条数据")
    @Log(title = "定时任务日志", businessType = BusinessType.INSERT)
    @PostMapping("/insert")
    @ResponseBody
    public ResultInfo<Object> insert(@RequestBody QuartzLog quartzLog){
        boolean result = quartzLogService.save(quartzLog);
        if(result){
            return ResultInfo.success();
        }else{
            return ResultInfo.failed();
        }
    }

    /**
     * 编辑页面
     * @author Stranger.
     * @since 2021-08-10
     */
    @GetMapping("/edit/{id}")
    public String edit(Model model, @PathVariable("id") String id){
        QuartzLog quartzLog = quartzLogService.getById(id);
        model.addAttribute("quartzLog",quartzLog);
        return "job/quartzLog/edit";
    }

    /**
     * 更新一条数据
     * @param quartzLog
     * @return top.plgxs.common.api.ResultInfo<java.lang.Object>
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "更新定时任务日志", notes = "更新单条数据")
    @Log(title = "定时任务日志", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ResponseBody
    public ResultInfo<Object> update(@RequestBody QuartzLog quartzLog){
        if(quartzLog == null || StringUtils.isBlank(quartzLog.getId())){
            return ResultInfo.validateFailed();
        }
        boolean result = quartzLogService.updateById(quartzLog);
        if(result){
            return ResultInfo.success();
        }else{
            return ResultInfo.failed();
        }
    }

    /**
     * 逻辑删除一条数据
     * @param id 主键
     * @return top.plgxs.common.api.ResultInfo<java.lang.Object>
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "删除定时任务日志", notes = "删除单条数据")
    @Log(title = "定时任务日志", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{id}")
    @ResponseBody
    public ResultInfo<Object> delete(@PathVariable("id") String id){
        if(StringUtils.isBlank(id)){
            return ResultInfo.validateFailed();
        }
        boolean result = quartzLogService.removeById(id);
        if(result){
            return ResultInfo.success();
        }else{
            return ResultInfo.failed();
        }
    }

    /**
     * 批量删除
     * @param ids id数组
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "批量删除定时任务日志", notes = "批量删除数据")
    @Log(title = "定时任务日志", businessType = BusinessType.DELETE)
    @PostMapping("/batchDelete")
    @ResponseBody
    public ResultInfo<Object> batchDelete(@RequestBody List<String> ids){
        boolean result = quartzLogService.removeByIds(ids);
        if(result){
            return ResultInfo.success("删除成功",null);
        }else{
            return ResultInfo.failed("删除失败");
        }
    }

    /**
     * 切换状态
     * @param id 主键
     * @param status 状态
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "切换定时任务日志状态", notes = "切换状态")
    @Log(title = "定时任务日志", businessType = BusinessType.SWITCH)
    @PostMapping("/switchStatus")
    @ResponseBody
    public ResultInfo<String> switchStatus(@RequestParam(name="id") String id, @RequestParam(name = "status") String status){
        QuartzLog quartzLog = new QuartzLog();
        quartzLog.setId(id);
        quartzLog.setStatus(status);
        boolean result = quartzLogService.updateById(quartzLog);
        if(result){
            return ResultInfo.success("切换成功",null);
        }else{
            return ResultInfo.failed("切换失败");
        }
    }
}
