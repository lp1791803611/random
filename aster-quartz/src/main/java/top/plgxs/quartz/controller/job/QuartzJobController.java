package top.plgxs.quartz.controller.job;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import top.plgxs.common.core.annotation.Log;
import top.plgxs.common.core.api.ResultInfo;
import top.plgxs.common.core.api.page.PageDataInfo;
import top.plgxs.common.core.constants.enums.BusinessType;
import top.plgxs.quartz.entity.job.QuartzJob;
import top.plgxs.quartz.service.job.QuartzJobService;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 定时任务 前端控制器
 * </p>
 *
 * @author Stranger.
 * @since 2021-08-10
 * @version 1.0
 */
@Api(tags = "定时任务管理")
@Controller
@RequestMapping("/quartzJob")
public class QuartzJobController {
    @Resource
    private QuartzJobService quartzJobService;

    /**
     * 定时任务页面
     * @author Stranger.
     * @since 2021-08-10
     */
    @GetMapping("/list")
    public String list(){
        return "job/quartzJob/list";
    }

    /**
     * 分页查询列表
     * @param searchParams 查询条件
     * @param pageNo 第几页
     * @param pageSize 每页几条
     * @return
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "分页查询定时任务列表", notes = "条件查询")
    @GetMapping("/pageList")
    @ResponseBody
    public ResultInfo<PageDataInfo> queryPageList(@RequestParam(name = "searchParams", required = false) String searchParams,
                                                    @RequestParam(name = "page", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "limit", defaultValue = "10") Integer pageSize){
        QueryWrapper<QuartzJob> queryWrapper = new QueryWrapper<>();
        if (StrUtil.isNotBlank(searchParams)) {
            JSONObject jsonObject = JSONObject.parseObject(searchParams);
            String jobName = String.valueOf(jsonObject.get("jobName"));
            if (StrUtil.isNotBlank(jobName)) {
                queryWrapper.eq("job_name", jobName);
            }
        }
        queryWrapper.orderByDesc("create_time");
        Page<QuartzJob> page = new Page<>(pageNo, pageSize);
        IPage<QuartzJob> pageList = quartzJobService.page(page, queryWrapper);
        return ResultInfo.success(new PageDataInfo<QuartzJob>(pageList.getRecords(),pageList.getTotal()));
    }

    /**
     * 添加页面
     * @author Stranger.
     * @since 2021-08-10
     */
    @GetMapping("/add")
    public String add(){
        return "job/quartzJob/add";
    }

    /**
     * 插入一条数据
     * @param quartzJob
     * @return top.plgxs.common.api.ResultInfo<java.lang.Object>
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "插入定时任务", notes = "插入单条数据")
    @Log(title = "定时任务", businessType = BusinessType.INSERT)
    @PostMapping("/insert")
    @ResponseBody
    public ResultInfo<Object> insert(@RequestBody QuartzJob quartzJob){
        boolean result = quartzJobService.save(quartzJob);
        if(result){
            return ResultInfo.success();
        }else{
            return ResultInfo.failed();
        }
    }

    /**
     * 编辑页面
     * @author Stranger.
     * @since 2021-08-10
     */
    @GetMapping("/edit/{id}")
    public String edit(Model model, @PathVariable("id") String id){
        QuartzJob quartzJob = quartzJobService.getById(id);
        model.addAttribute("quartzJob",quartzJob);
        return "job/quartzJob/edit";
    }

    /**
     * 更新一条数据
     * @param quartzJob
     * @return top.plgxs.common.api.ResultInfo<java.lang.Object>
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "更新定时任务", notes = "更新单条数据")
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ResponseBody
    public ResultInfo<Object> update(@RequestBody QuartzJob quartzJob){
        if(quartzJob == null || StringUtils.isBlank(quartzJob.getId())){
            return ResultInfo.validateFailed();
        }
        boolean result = quartzJobService.updateById(quartzJob);
        if(result){
            return ResultInfo.success();
        }else{
            return ResultInfo.failed();
        }
    }

    /**
     * 逻辑删除一条数据
     * @param id 主键
     * @return top.plgxs.common.api.ResultInfo<java.lang.Object>
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "删除定时任务", notes = "删除单条数据")
    @Log(title = "定时任务", businessType = BusinessType.DELETE)
    @GetMapping("/delete/{id}")
    @ResponseBody
    public ResultInfo<Object> delete(@PathVariable("id") String id){
        if(StringUtils.isBlank(id)){
            return ResultInfo.validateFailed();
        }
        boolean result = quartzJobService.removeById(id);
        if(result){
            return ResultInfo.success();
        }else{
            return ResultInfo.failed();
        }
    }

    /**
     * 批量删除
     * @param ids id数组
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "批量删除定时任务", notes = "批量删除数据")
    @Log(title = "定时任务", businessType = BusinessType.DELETE)
    @PostMapping("/batchDelete")
    @ResponseBody
    public ResultInfo<Object> batchDelete(@RequestBody List<String> ids){
        boolean result = quartzJobService.removeByIds(ids);
        if(result){
            return ResultInfo.success("删除成功",null);
        }else{
            return ResultInfo.failed("删除失败");
        }
    }

    /**
     * 切换状态
     * @param id 主键
     * @param status 状态
     * @author Stranger.
     * @since 2021-08-10
     */
    @ApiOperation(value = "切换定时任务状态", notes = "切换状态")
    @Log(title = "定时任务", businessType = BusinessType.SWITCH)
    @PostMapping("/switchStatus")
    @ResponseBody
    public ResultInfo<String> switchStatus(@RequestParam(name="id") String id, @RequestParam(name = "status") String status){
        QuartzJob quartzJob = new QuartzJob();
        quartzJob.setId(id);
        quartzJob.setStatus(status);
        boolean result = quartzJobService.updateById(quartzJob);
        if(result){
            return ResultInfo.success("切换成功",null);
        }else{
            return ResultInfo.failed("切换失败");
        }
    }
}
