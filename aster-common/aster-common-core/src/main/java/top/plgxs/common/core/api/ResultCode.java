package top.plgxs.common.core.api;

/**
 * <p>常用API操作码</p>
 *
 * @author Stranger。
 * @version 1.0
 * @since 2020/12/23 15:30
 */
public enum ResultCode {
    /**
     * 200-操作成功
     */
    SUCCESS(200, "操作成功"),
    /**
     * 500-操作失败
     */
    FAILED(500, "操作失败"),
    /**
     * 404-参数检验失败
     */
    VALIDATE_FAILED(404, "参数检验失败"),
    /**
     * 401-暂未登录或token已经过期
     */
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    /**
     * 403-没有相关权限,请联系管理员
     */
    FORBIDDEN(403, "没有相关权限,请联系管理员");


    private Integer code;
    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
