package top.plgxs.common.core.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import top.plgxs.common.core.api.ResultInfo;

import java.io.FileNotFoundException;

/**
 * 全局异常处理
 * @author Stranger。
 * @version 1.0
 * @since 2021/3/15 21:30
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler(value = BusinessException.class)
    public ResultInfo handle(BusinessException e) {
        if (e.getErrorCode() != null) {
            return ResultInfo.failed(e.getErrorCode(), e.getMessage());
        }
        return ResultInfo.failed(e.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResultInfo handleValidException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField()+fieldError.getDefaultMessage();
            }
        }
        return ResultInfo.validateFailed(message);
    }

    @ResponseBody
    @ExceptionHandler(value = BindException.class)
    public ResultInfo handleValidException(BindException e) {
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField()+fieldError.getDefaultMessage();
            }
        }
        return ResultInfo.validateFailed(message);
    }

    @ExceptionHandler({FileNotFoundException.class, NoHandlerFoundException.class})
    public ModelAndView noFoundException(Exception exception) {
        log.error("程序异常==>errorCode:{}, exception:{}", HttpStatus.NOT_FOUND.value(), exception);
        ModelAndView mav = new ModelAndView("error/404");
        mav.addObject(ResultInfo.failed(HttpStatus.NOT_FOUND.value(),exception.getMessage()));
        return mav;
    }

    /**
     * NullPointerException 空指针异常捕获处理
     * @param ex 自定义NullPointerException异常类型
     * @return Result
     */
    @ExceptionHandler
    public ModelAndView handleException(NullPointerException ex) {
        log.error("程序异常：{}" + ex.toString());
        ModelAndView mav = new ModelAndView("error/500");
        mav.addObject(ResultInfo.failed(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage()));
        return mav;
    }
}
