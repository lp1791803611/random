package top.plgxs.common.core.constants.enums;

/**
 * 性别
 * @author Stranger。
 * @version 1.0
 * @since 2021/11/5 14:27
 */
public enum GenderEnum {
    /**
     * 男
     */
    Man("M", "男"),
    /**
     * 女
     */
    Women("F", "女");


    private String code;
    private String message;

    GenderEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String getCode(String value) {
        GenderEnum[] enums = GenderEnum.values();
        for (GenderEnum bEnum : enums) {
            if (bEnum.getMessage().equals(value)) {
                return bEnum.getCode();
            }
        }
        return null;
    }

    public static String getMessage(String code) {
        GenderEnum[] enums = GenderEnum.values();
        for (GenderEnum bEnum : enums) {
            if (bEnum.getCode().equals(code)) {
                return bEnum.getMessage();
            }
        }
        return null;
    }

    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
